import Link from './components/Link'
import View from './components/View'
import KeepAlive from './components/keep-alive'

export function install (Vue) {
  Vue.mixin({
    beforeCreate () {
      if (this.$options.router) {
        this._routerRoot = this
        this._router = this.$options.router
        this._router.init(this)
        Vue.util.defineReactive(this, '_route', this._router.history.current)
      } else {
        this._routerRoot = (this.$parent && this.$parent._routerRoot) || this
      }
      Object.defineProperty(this, '$router', {
        get () {
          return this._routerRoot._router
        }
      })

      Object.defineProperty(this, '$route', {
        get () {
          return this._routerRoot._route
        }
      })
    }
  })

  Vue.component('router-view', View)
  Vue.component('router-link', Link)
  Vue.component('keep-alive-janlay', KeepAlive)
}
