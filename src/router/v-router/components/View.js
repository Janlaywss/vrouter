export default {
  name: 'RouteView',
  functional: true,
  render (_, { parent }) {
    const h = parent.$createElement
    const current = parent.$route
    const { nameMap, pathMap } = parent.$router.matchers

    let components = null
    if (current.name) {
      components = nameMap[current.name].components.default
    } else {
      components = pathMap[current.path].components.default
    }

    return h(components)
  }
}
