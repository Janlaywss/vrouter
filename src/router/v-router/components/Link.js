export default {
  name: 'RouterLink',
  props: {
    to: {}
  },
  methods: {
    handleClick (e) {
      e.preventDefault()
      const { to } = this.$props
      this.$router.push(to)
    }
  },
  render (h) {
    return <a onClick={this.handleClick}>{ this.$slots.default }</a>
  }
}
