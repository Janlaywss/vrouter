import { extend } from './misc'

export function normalizeLocation (raw) {
  let next = typeof raw === 'string' ? { path: raw } : raw
  if (next._normalized) {
    return next
  } else if (next.name) {
    next = extend({}, raw)
    const params = next.params
    if (params && typeof params === 'object') {
      next.params = extend({}, params)
    }
    return next
  }

  return {
    _normalized: true,
    ...next
  }
}
