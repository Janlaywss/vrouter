import { setStateKey, getStateKey } from '../utils/state-key'

export function pushState (url) {
  const { history } = window
  history.pushState({ key: setStateKey(getStateKey()) }, '', url)
}
