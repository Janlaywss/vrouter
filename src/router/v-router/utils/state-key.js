const Time = window.performance

let _key = getStateKey()

export function getStateKey () {
  return Time.now().toFixed(3)
}

export function setStateKey (key) {
  return (_key = key)
}
