export function runQueue (queue, fn, cb) {
  const step = index => {
    // 如果index >= 队列长度，直接执行cb()，完成函数调用
    // 这是为了弥补执行到队尾时，获取step(index+1)出错导致
    if (index >= queue.length) {
      cb()
    } else {
      if (queue[index]) {
        // 执行迭代器，开始处理逻辑。
        // 第一个参数是当前执行的queue，第二个是下一个queue。方便iterator的栈调用
        fn(queue[index], () => {
          step(index + 1)
        })
      } else {
        // 如果当前执行的函数没有，就走下一个
        step(index + 1)
      }
    }
  }
  // 开始执行队列中的函数
  step(0)
}
