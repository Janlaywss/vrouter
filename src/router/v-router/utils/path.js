export function cleanPath (path) {
  // 把 链接中的 "\" 移除掉
  return path.replace(/\/\//g, '/')
}
