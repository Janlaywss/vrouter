import History from './base'
import { pushState } from '../utils/push-state'

export default class HashHistory extends History {
  constructor (router, { base }) {
    super(router, base)
    this.initEventLister()
  }

  initEventLister () {
    if (!location.hash) {
      location.hash = '/'
    }

    window.addEventListener('hashchange', (e) => {
      this.current = location.hash.slice(1)
    })
  }

  push (location, onComplete) {
    this.transitionTo(location, () => {
      pushHash(location)
      onComplete && onComplete()
    })
  }
}

function pushHash (path) {
  window.location.hash = path
}
